/*Configuracion de servidor node*/
const market = require('./market');
var express = require('express');
var bodyparser = require('body-parser');
var favicon = require('serve-favicon');
var logger = require('morgan');
var path = require('path');
var io = require('socket.io');
var fs = require( 'fs' );
var https = require('https');
var mysql = require('mysql');
var timer = require('timers');
var nodemailer = require('nodemailer');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'Mailencita2612/',
  database : 'domo'
});

//Importo las rutas de la pagina index publica
var routesPublic  = require('./routes/indexAngular');

//Importo las rutas de la pagina admin privada
var routesPrivate = require('./routes/adminAngular');

var app = express();
var http = require('http').Server(app);


app.use(bodyparser.json({limit: '50mb'}));

// Se leventa el servidor para socket.io
var servers = require('http').Server(app);
servers.listen(3001);

var sio;
sio = io.listen(servers)
console.log('Socket escuchando en el puerto numero ' + servers.address().port);

var datos = [{'id':1, 'fecha':'111111', 'hora':'222222', 'nombre':'Temperatura', 'tipo':'bar', 'valor':0}];


//for (i=0; i < 100; i++)
//{
    //data.valor = i;
    //console.log(datos.id);
    //sio.sockets.emit("sensor", datos);
//}

setInterval(function () {
  //market.updateMarket();
  var aleatorio = Math.round(Math.random()*100);
  datos.valor = aleatorio;
  sio.sockets.emit('sensor', datos);
}, 1000);


app.get('/api/market', (req, res) => {
  res.send(market.marketPositions);
});


app.post('/estado', function(req, res, next) {
    var consulta = req.body;
    console.log('request received socketid:', req.body.socketid, 'request received estado:', req.body.estado, 'request received idLuminaria:', req.body.idLuminaria);
    var query = connection.query('UPDATE luminaria SET estado = ? WHERE id = ?', [consulta.estado, consulta.idLuminaria], function (err, result)
    {
      if (err){
          console.error(err);
          return res.send(err);
      }
      else
      {
          return res.send('Estado recibido OK');
          console.error(res.send('Estado recibido OK'));
      }
    })
})

app.post('/estadoFromArduino', function(req, res, next) {
    var consulta = req.body;
    sio.sockets.emit("receiveEstado", req.body.estado);
})

app.get('/getTituloHeader', function(req, res, next)
{
  //connection.query('select * from cabecera', function(err, rows, fields)
  //{
    //if (err) throw err;
    //res.setHeader('content-type', 'application/json');
    //res.json(rows);
  //});
});

// Se define el directorio de las vistas
app.set('views', [path.join(__dirname, 'views'), path.join(__dirname, 'admin/templates')]);

//Motor de vistas html
app.engine("html", require("ejs").renderFile);
app.set('view engine', 'html');

//Configuracion del puerto del servidor node
//app.set('port', process.env.PORT || 8080);

// Inicia el servidor node para escuchar Rest
var server = app.listen(process.env.PORT || '8085', function ()
{
  console.log('Servidor escuchando en puerto %s', server.address().port);
  console.log('Press Ctrl+C to quit.');
}
);

//Define rutas estaticas para la parte publica del sitio
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routesPublic);

//Define rutas estaticas para la parte privada del sitio
app.use(express.static(path.join(__dirname, 'admin')));
app.use('/', routesPrivate);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Recurso no encontrado');
    err.status = 404;
    next(err);
});

// error handlers

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


// Se leventa el servidor para socket.io
//var servers = require('http').Server(app);
//servers.listen(3001);

//var sio;
//sio = io.listen(servers)
//console.log('Socket escuchando en el puerto numero ' + servers.address().port);

//Esta funcion recibe cada una de las conexiones
//de los browser's de internet, a la pagina principal del sitio
//Index.html

//setInterval(function () {
  //market.updateMarket();
  //sio.sockets.emit('market', market.marketPositions[0]);
//}, 5000);

sio.on('connection', function (socket) {
  console.log('a user connected');
});

sio.sockets.on('connection', function (socket) {

    console.log("Nuevo usuario conectado");

    socket.on('disconnect', () =>
    {
      console.log("Usuario desconectado");
    }
    )

    socket.on('enviaEstadoSocket', function(data)
    {
        console.log(data.id + " " + data.estado);

        //socket.broadcast.emit('mensajeDF', data.msg);
    });

    socket.on('enviaRespuesta', function(data)
    {
        socket.broadcast.to(data.id).emit('respuestaDF', data.id, data.msg);
    });
});
