var numMsg = 0;
var numMsgResp = 0;

function openChat()
{
		//Desahbilitar objetos del chat inactivo
    $("#chatFooterOpen").removeClass("footerChatNoneOpen");
    $("#contactOnLine").removeClass("footerChatTextNone");

    //Invisibilizar objetos del chat inactivo
		$("#chatFooterOpen").addClass("footerChatElementNone");
    $("#contactOnLine").addClass("footerChatElementNone");


		//Habilitar objetos del chat activo
		$("#chatHead").removeClass("footerChatElementNone");
		$("#chatBody").removeClass("footerChatElementNone");
		$("#chatFooter").removeClass("footerChatElementNone");
    $("#txtWriteMsg").removeClass("footerChatElementNone");
		$("#btnSendMsg").removeClass("footerChatElementNone");

		$("#chatHead").addClass("footerChatHeader");
		$("#chatBody").addClass("footerChatBody");
		$("#chatFooter").addClass("footerChatFooter");
		$("#txtWriteMsg").addClass("form-control");
		$("#btnSendMsg").addClass("btn btn-default btn-aylla");
}

function closeChat()
{

  //Habiliat objetos del chat inactivo
  $("#contactOnLine").removeClass("footerChatElementNone");
  $("#chatFooterOpen").removeClass("footerChatElementNone");

  $("#contactOnLine").addClass("footerChatTextNone");
  $("#chatFooterOpen").addClass("footerChatNoneOpen");



  $("#chatHead").removeClass("footerChatHeader");
  $("#chatBody").removeClass("footerChatBody");
  $("#chatFooter").removeClass("footerChatFooter");
  $("#txtWriteMsg").removeClass("form-control");
  $("#btnSendMsg").removeClass("btn btn-default btn-aylla");

  //Habilitar elementos de clase chat Visible
  $("#chatHead").addClass("footerChatElementNone");
  $("#chatBody").addClass("footerChatElementNone");
  $("#chatFooter").addClass("footerChatElementNone");
  $("#txtWriteMsg").addClass("footerChatElementNone");
  $("#btnSendMsg").addClass("footerChatElementNone");
}


function dibujarMensajeRespuesta()
{
  var buscaPanelUsuario = $("#mensajesEscritos").find(".userConversacionVisible");
  var idPanel = buscaPanelUsuario.attr('id');


  numMsgResp = numMsgResp + 1;
  //Se crea el div para el mensaje de respuesta
  divMsgReps = "<div id='MsgReps"+ idPanel + numMsgResp + "' class='footerChatMessageAdm'></div><br />";

  //Se agrega el div al panel del usuario activo
  $("#" + idPanel + "").append(divMsgReps);

  var msg = $("#msgAdmin").val();

  //Se agrega el contenido del mensaje, al div del mensaje
  $("#MsgReps"+ idPanel + numMsgResp + "").append(msg);

  //Se rescata el id del usuario
  largo = idPanel.length;
  var id = idPanel.substring(4, largo)

  enviaRespuesta(msg, id);
}

//function dibujarMensaje()
//{
//}

//function enviaConsulta(mensaje)
//{

//}

function enviaRespuesta(mensaje, id)
{

  //Puerto de escucha del socket
  //var socket = io.connect('http://localhost:3001');
  var socket = io.connect('http://159.203.168.146:3001');

  //Se envia el mensaje al servidor node
  socket.emit('enviaRespuesta', { id: id, msg:mensaje });

}

function chatWithUser(selector)
{
  var currentClass = $(selector).attr("class");

  if(currentClass == "userConectedGral" || currentClass == "userConectedGralAlert")
  {
    $("#conv" + selector.id + "").removeClass("userConversacion");
    $("#conv" + selector.id + "").addClass("userConversacionVisible");
    $(selector).removeClass(currentClass);
    $(selector).addClass("userConectedContact");
  }
  else
  {
    $("#conv" + selector.id + "").removeClass("userConversacionVisible");
    $("#conv" + selector.id + "").addClass("userConversacion");
    $(selector).removeClass(currentClass);
    $(selector).addClass("userConectedGral");
  }
}
