var indexAngular = angular.module("indexAngular", ['ngRoute', 'ui.bootstrap', 'ui.bootstrap.showErrors']);


/*Configuracion de la pagina publica*/

/*Configuracion de rutas*/
indexAngular.config(function($routeProvider)
{
    $routeProvider

    .when("/", {
        templateUrl : "templates/inicio.html",
        controller : "InicioCtrl"
    })
    .when("/nosotros", {
        templateUrl: "templates/nosotros.html",
        controller: "NosotrosCtrl"
    })
    .when("/productos", {
        templateUrl: "templates/productos.html",
        controller: "ProductosCtrl"
    })
    .when("/servicios", {
        templateUrl: "templates/servicios.html",
        controller: "ServiciosCtrl"
    })
    .when("/clientes", {
        templateUrl: "templates/clientes.html",
        controller: "ClientesCtrl"
    })
    .when("/partners", {
        templateUrl: "templates/partners.html",
        controller: "PartnersCtrl"
    })
    .when("/admin", {
        templateUrl: "templates/administracion.html",
        controller: "AdminCtrl"
    })
    .otherwise({ redirectTo : "/" });
})

//app.controller('ModalCtrl', function ($scope, $modal, $log, $http) {
indexAngular.controller('InicioCtrl', function($scope, $http) {
        //Puerto de escucha del socket
        var socket = io.connect('http://localhost:3001');

        $scope.OnOff = function (){

            let element = document.getElementById('btnOnOf');
            if(element.className == "btn btn-light")
            {
                element.className = "btn btn-success";
                $scope.enviaEstado(1);
            }
            else
            {
                element.className = "btn btn-light";
                $scope.enviaEstado(0);
            }
        };

        $scope.enviaEstado = function (estado){

          //Se guarda el id del socket del navegador
          var socketid = socket.id;

          //Se envia el socket id y el estado del objeto al servidor node
          socket.emit('enviaEstadoSocket', { id: socketid, estado:estado });

          //Se envia el estado al servidor node via rest
          $http
          ({
            url: 'http://localhost:8085/estado',
            method: 'POST',
            data: JSON.stringify({socketid:socketid, estado:estado, idLuminaria:'1'})
          }).then
          (
            function (httpResponse)
            {
              console.log('response:', httpResponse);
            }
          )
        };

        socket.on('receiveEstado', function (estado)
        {
          console.log("EstadofromNodeServer: " + estado);

          let element = document.getElementById('btnOnOf');
          if(estado.toString() == 1)
          {
            element.className = "btn btn-success";
          }
          else
          {
            element.className = "btn btn-light";
          }
        });
    }
);
