var express = require('express');
var router = express.Router();

router.get('/admin', function(req, res, next) {
  res.render('admin', { title: 'Backend Admin' });
});

router.get('/ChatServerOnline', function(req, res, next) {
  res.render('ChatServerOnline', { title: 'Clientes Online' });
});

module.exports = router;