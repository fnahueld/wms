var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Tecnologia de Informacion Sustentable' });
});

//router.get('/private', function(req, res) {
 //res.send('¡Soy el panel de administración!');
//});

module.exports = router;
